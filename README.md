# Hugo secure resource module
This module provides partials to collect, fingerprint, assemble, link, and
render resources that may be loaded from or inlined into your HTML pages.
From that it generates a content security policy (CSP) that can be rendered
in the page header. A CDN host and a separate font host can be configured.

The module is capable of handling the following resource types

* javascript
* CSS
* font
